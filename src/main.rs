use std::time::Duration;

use chrono::{DateTime, Local};
use tokio_cron_scheduler::{Job, JobScheduler, JobSchedulerError};

#[tokio::main]
async fn main() -> Result<(), JobSchedulerError> {
    let mut sched = JobScheduler::new().await?;

    // Add basic cron job
    sched.add(
        Job::new("1/10 * * * * *", |_uuid, _l| {
            let now: DateTime<Local> = Local::now();
            let formatted = now.format("%Y-%m-%d %H:%M:%S").to_string();
            println!("{}", formatted);
        })?
    ).await?;

    sched.start().await?;
    // tokio::time::sleep(Duration::from_secs(100)).await;
    // Ok(())
    loop {
        tokio::time::sleep(Duration::from_secs(1)).await;
    }
}
